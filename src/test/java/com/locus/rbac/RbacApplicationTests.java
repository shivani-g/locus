package com.locus.rbac;

import com.locus.rbac.core.AccessManager;
import com.locus.rbac.core.VolatileDataManager;
import com.locus.rbac.core.entity.Resource;
import com.locus.rbac.core.entity.Role;
import com.locus.rbac.exception.InvalidDataException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Collections;

@SpringBootTest
        // needed?
class RbacApplicationTests {
    private AccessManager accessManager;


    @BeforeEach
    void init() {
        accessManager = new AccessManager(new VolatileDataManager());
    }

    @Test
    void assignValidRoleToUser() {
        Assertions.assertThat(accessManager.assignRoleToUser("GSM", 1L))
                .isEqualTo(Collections.singletonList(new Role("GSM", "Gift Shop Manager")));
    }

    @Test
    void assignInvalidRoleToUser() {
        Assertions.assertThatThrownBy(
                () -> accessManager.assignRoleToUser("GKM", 1L))
                .isInstanceOf(InvalidDataException.class).hasMessage("incorrect role");
    }

    @Test
    void assignAlreadyAssignedRoleToUser() {
        accessManager.assignRoleToUser("GSM", 1L);
        Assertions.assertThat(accessManager.assignRoleToUser("GSM", 1L))
                .isEqualTo(Collections.singletonList(new Role("GSM", "Gift Shop Manager")));
    }

    @Test
    void removeValidRoleFromUser() {
        accessManager.assignRoleToUser("GSM", 1L);
        accessManager.assignRoleToUser("NAdmin", 1L);
        accessManager.assignRoleToUser("AM", 1L);
        Assertions.assertThat(accessManager.removeRoleForUser("GSM", 1L))
                .isEqualTo(Arrays.asList(
                        new Role("NAdmin", "Newsletter Admin"),
                        new Role("AM", "Area Manager")));
    }

    @Test
    void removeInvalidRoleFromUser() {
        accessManager.assignRoleToUser("GSM", 1L);
        accessManager.assignRoleToUser("NAdmin", 1L);
        accessManager.assignRoleToUser("AM", 1L);
        Assertions.assertThatThrownBy(
                () -> accessManager.removeRoleForUser("RM", 1L))
                .isInstanceOf(InvalidDataException.class).hasMessage("incorrect role");
    }

    @Test
    void removeNonExistentRoleFromUser() {
        accessManager.assignRoleToUser("GSM", 1L);
        accessManager.assignRoleToUser("NAdmin", 1L);
        Assertions.assertThat(accessManager.removeRoleForUser("AM", 1L))
                .isEqualTo(Arrays.asList(
                        new Role("GSM", "Gift Shop Manager"),
                        new Role("NAdmin", "Newsletter Admin")));
    }

    @Test
    void grantPermissionToUnAuthorizedUser() {
        accessManager.assignRoleToUser("GSM", 1L);
        Assertions.assertThat(accessManager.isAuthorised(1L, "EDIT", Resource.CUSTOMER_PROFILE))
                .isEqualTo(false);
    }

    @Test
    void grantPermissionToAuthorizedUser() {
        accessManager.assignRoleToUser("GSM", 1L);
        Assertions.assertThat(accessManager.isAuthorised(1L, "READ", Resource.CUSTOMER_PROFILE))
                .isEqualTo(true);
    }

    @Test
    void grantPermissionForUndefinedAction() {
        accessManager.assignRoleToUser("GSM", 1L);
        Assertions.assertThatThrownBy(
                () -> accessManager.isAuthorised(1L, "APPROVE", Resource.CUSTOMER_PROFILE))
                .isInstanceOf(InvalidDataException.class).hasMessage("incorrect action");
    }


}
