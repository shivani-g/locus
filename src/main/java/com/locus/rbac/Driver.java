package com.locus.rbac;

import com.locus.rbac.core.AccessManager;
import com.locus.rbac.core.VolatileDataManager;
import com.locus.rbac.core.entity.Resource;
import com.locus.rbac.exception.InvalidDataException;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Driver {
    private final AccessManager accessManager;

    public Driver() {
        accessManager = new AccessManager(new VolatileDataManager());
    }

    public void run() {
        System.out.println("Instructions for commands can be found in README.md");
        while (true) {
            System.out.println("Enter command");
            Scanner in = new Scanner(System.in);
            String input = in.nextLine();
            String[] commandArgs = input.split(" ");
            String action = commandArgs[0];
            List<String> args = Arrays.asList(commandArgs).subList(1, commandArgs.length);
            Object response = null;
            try {
                response = executeCommand(action, args);
            } catch (InvalidDataException ex) {
                response = ex;
            } catch (NumberFormatException ex) {
                response = ex;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            System.out.println(response);
        }
    }

    private Object executeCommand(String action, List<String> args) {
        Object response;
        switch (action) {
            case "ASSIGN":
                response = this.accessManager.assignRoleToUser(args.get(0), Long.parseLong(args.get(1)));
                break;
            case "REMOVE":
                response = this.accessManager.removeRoleForUser(args.get(0), Long.parseLong(args.get(1)));
                break;
            case "IS_AUTHORISED":
                response = this.accessManager.isAuthorised(
                        Long.parseLong(args.get(0)), args.get(1), Resource.valueOf(args.get(2)));
                break;
            case "VIEW_USERS":
                response = this.accessManager.getAllUsers();
                break;
            case "VIEW_ROLES":
                response = this.accessManager.getValidRoles();
                break;
            case "VIEW_ACTIONS":
                response = this.accessManager.getValidActions();
                break;
            case "VIEW_USER_TO_ROLES":
                response = this.accessManager.getUserToRolesMap();
                break;
            case "VIEW_ROLE_TO_PERMS":
                response = this.accessManager.getRoleToPermissionsMap();
                break;
            case "VIEW_RESOURCES":
                response = Arrays.stream(Resource.values()).map(Resource::name).collect(Collectors.toList());
                break;
            default:
                response = new RuntimeException("Invalid input");
        }
        return response;
    }

    public static void main(String[] args) {
        Driver d = new Driver();
        try {
            d.run();
        } catch (Exception ex) {
            System.out.println(ex);
        }

    }
}
