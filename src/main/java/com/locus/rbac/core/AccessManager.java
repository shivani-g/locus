package com.locus.rbac.core;

import com.locus.rbac.core.entity.*;
import com.locus.rbac.exception.InvalidDataException;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class AccessManager {
    private final DataManager dataManager;

    public AccessManager(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    public List<Role> assignRoleToUser(String roleCode, Long userId) {
        dataManager.getRoleFromCode(roleCode).orElseThrow(
                () -> new InvalidDataException("incorrect role")
        );
        dataManager.assignRoleToUser(roleCode, userId);
        return getRolesForUser(userId);
    }

    private List<Role> getRolesForUser(Long userId) {
        return dataManager.getRolesForUser(userId);
    }

    public List<Role> removeRoleForUser(String roleCode, Long userId) {
        dataManager.getRoleFromCode(roleCode).orElseThrow(
                () -> new InvalidDataException("incorrect role")
        );
        dataManager.removeRoleFromUser(roleCode, userId);
        return getRolesForUser(userId);
    }

    public Boolean isAuthorised(Long userId, String actionCode, Resource resource) {

        Environment e = new Environment(resource,
                dataManager.getActionFromCode(actionCode)
                        .orElseThrow(
                                () -> new InvalidDataException("incorrect action")
                        ), userId);
        return !CollectionUtils.isEmpty(dataManager.getPermissionsFromEnvironment(e));
    }

    public List<User> getAllUsers() {
        return dataManager.getUsers();
    }

    public List<Action> getValidActions(){
        return dataManager.getValidActions();
    }
    public List<Role> getValidRoles(){
        return dataManager.getValidRoles();
    }
    public Map<Long, Set<String>> getUserToRolesMap(){
        return dataManager.getUserToRolesMap();
    }
    public Map<String, List<Permission>> getRoleToPermissionsMap(){
        return dataManager.getRoleToPermissionsMap();
    }
}
