package com.locus.rbac.core;

import com.locus.rbac.core.entity.*;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;

public class VolatileDataManager implements DataManager {
    private Map<String, List<Permission>> roleToPermissionsMap;
    private Map<Long, User> users;
    private Map<Long, Set<String>> userToRolesMap;
    private Map<String, Action> actions;
    private Map<String, Role> roles;

    public VolatileDataManager() {
        // Add users
        users = new HashMap<>();
        addDefaultUsers();

        // Add actions
        actions = new HashMap<>();
        addDefaultActions();

        // Add roles and permissions
        roles = new HashMap<>();
        roleToPermissionsMap = new HashMap<>();
        addRolesAndPermissionsForSampleBusiness1();

        // Initialize user to roles map
        userToRolesMap = new HashMap<>();
    }

    private void addRolesAndPermissionsForSampleBusiness1() {
        roles.put("GSM", new Role("GSM", "Gift Shop Manager"));
        roles.put("AM", new Role("AM", "Area Manager"));
        roles.put("NAdmin", new Role("NAdmin", "Newsletter Admin"));

        // Add permissions for the role of GIFT_SHOP_MANAGER
        List<Permission> permissions = new ArrayList<>();
        permissions.add(new Permission("READ", Resource.CATALOG_ITEM));
        permissions.add(new Permission("READ", Resource.CUSTOMER_PROFILE));
        permissions.add(new Permission("CREATE", Resource.INVOICE));
        roleToPermissionsMap.put("GSM", permissions);

        // Add permissions for the role of GIFT_SHOP_MANAGER
        permissions = new ArrayList<>();
        permissions.add(new Permission("CREATE", Resource.NEWSLETTER));
        permissions.add(new Permission("EDIT", Resource.NEWSLETTER));
        permissions.add(new Permission("DELETE", Resource.NEWSLETTER));
        permissions.add(new Permission("SEND", Resource.NEWSLETTER));
        permissions.add(new Permission("EDIT", Resource.DISTRIBUTION_LIST));
        roleToPermissionsMap.put("NAdmin", permissions);
    }

    private void addDefaultActions() {
        actions.put("READ", new Action("READ", "read"));
        actions.put("CREATE", new Action("CREATE", "create"));
        actions.put("EDIT", new Action("EDIT", "edit"));
        actions.put("DELETE", new Action("DELETE", "delete"));
        actions.put("SEND", new Action("SEND", "send"));
    }

    private void addDefaultUsers() {
        users.put(1L, new User(1L, "u1"));
        users.put(2L, new User(2L, "u2"));
        users.put(3L, new User(3L, "u3"));
        users.put(4L, new User(4L, "u4"));
    }


    /**
     * @param roleCode
     * @param userId
     * @return True if the role does not exist for the given user and has been assigned.
     * Returns False if the role already exists for the given user
     */
    @Override
    public Boolean assignRoleToUser(String roleCode, Long userId) {
        if (!userToRolesMap.containsKey(userId)) {
            userToRolesMap.put(userId, new HashSet<>());
        }
        if (userToRolesMap.get(userId).contains(roleCode)) {
            return Boolean.FALSE;
        }
        userToRolesMap.get(userId).add(roleCode);
        return Boolean.TRUE;
    }

    /**
     * @param roleCode
     * @param userId
     * @return True if the role exists for the given user and has been removed.
     * Returns False if the role does not exist for the given user
     */
    @Override
    public Boolean removeRoleFromUser(String roleCode, Long userId) {
        if (userToRolesMap.containsKey(userId) && userToRolesMap.get(userId).contains(roleCode)) {
            userToRolesMap.get(userId).remove(roleCode);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public List<Role> getRolesForUser(Long userId) {
        return userToRolesMap.get(userId).stream().map(roleId -> roles.get(roleId)).collect(Collectors.toList());
    }

    @Override
    public List<Permission> getPermissionsFromEnvironment(Environment e) {
        if (!userToRolesMap.containsKey(e.getUserId())) {
            return Collections.emptyList();
        }
        return userToRolesMap.get(e.getUserId())
                .stream()
                .filter(r -> roleToPermissionsMap.containsKey(r))
                .map(r -> roleToPermissionsMap.get(r))
                .flatMap(List::stream)
                .filter(p -> p.getResource().equals(e.getResource())
                        && p.getActionCode().equals(e.getAction().getCode())
                        && p.getIsEnabled()).collect(Collectors.toList());
    }

    @Override
    public Optional<Action> getActionFromCode(String code) {
        return actions.containsKey(code) ? Optional.of(actions.get(code)) : Optional.empty();
    }

    public Optional<Role> getRoleFromCode(String code) {
        return roles.containsKey(code) ? Optional.of(roles.get(code)) : Optional.empty();
    }

    @Override
    public void addUser(User u) {
        // TODO: implement
    }

    @Override
    public void addPermission(Permission p) {
        // TODO: implement
    }

    @Override
    public void addRole(Role r) {
        // TODO: implement
    }

    @Override
    public List<User> getUsers() {
        return new ArrayList<>(this.users.values());
    }

    @Override
    public List<Action> getValidActions() {
        return new ArrayList<>(this.actions.values());
    }

    @Override
    public List<Role> getValidRoles() {
        return new ArrayList<>(this.roles.values());
    }
    @Override
    public Map<Long, Set<String>> getUserToRolesMap() {
        return this.userToRolesMap;
    }

    @Override
    public Map<String, List<Permission>> getRoleToPermissionsMap() {
        return this.roleToPermissionsMap;
    }
}
