package com.locus.rbac.core;

import com.locus.rbac.core.entity.*;

import java.util.*;

public interface DataManager {

    void addUser(User u);

    void addPermission(Permission p);

    void addRole(Role r);

    Boolean assignRoleToUser(String roleCode, Long userId);

    Boolean removeRoleFromUser(String roleCode, Long userId);

    List<Role> getRolesForUser(Long userId);

    List<Permission> getPermissionsFromEnvironment(Environment e);

    Optional<Action> getActionFromCode(String code);

    Optional<Role> getRoleFromCode(String code);

    List<User> getUsers();

    List<Action> getValidActions();

    List<Role> getValidRoles();

    Map<Long, Set<String>> getUserToRolesMap();

    Map<String, List<Permission>> getRoleToPermissionsMap();



}
