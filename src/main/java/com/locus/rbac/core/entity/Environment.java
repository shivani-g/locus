package com.locus.rbac.core.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Environment {
    private Resource resource;
    private Action action;
    private Long userId;
}
