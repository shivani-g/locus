package com.locus.rbac.core.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class Permission {
    private String actionCode;
    private Resource resource;
    private Boolean isEnabled;

    public Permission(String actionCode, Resource resource) {
        this.actionCode = actionCode;
        this.resource = resource;
        this.isEnabled = Boolean.TRUE;
    }
}
