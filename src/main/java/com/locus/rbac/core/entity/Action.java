package com.locus.rbac.core.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class Action {
    private String code;
    private String description;
}
