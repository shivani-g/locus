package com.locus.rbac.core.entity;

import lombok.ToString;

@ToString
public enum Resource {
    CATALOG_ITEM, CUSTOMER_PROFILE, NEWSLETTER, DISTRIBUTION_LIST, INVOICE
}
