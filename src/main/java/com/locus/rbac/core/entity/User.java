package com.locus.rbac.core.entity;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class User {
    private Long id;
    private String name;
}
