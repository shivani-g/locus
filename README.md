# Role Based Access Control System

## Requirements
* Java

### To Run
* to build and run tests: 
    * ./gradlew clean build
* to run: 
    * ./gradlew clean bootRun -q --console=plain
    * starts an interactive application which takes input from the commandline.


### Core components
1. Driver.java: 
    * runs an interactive commandline application, to take three types of commands entered by the user. 
    * interacts with the AccessManager to run the commands
2. AccessManager.java: 
    * performs authorization for a user. 
    * interacts with DataManager to access the role and permission info stored for users
3. DataManager.java:
    * stores users, roles, user to roles mappings, role to permissions mappings and valid actions
4. Permission
    * a combination of action and resource
    
### Assumptions
1. Authentication has already been carried out before the authorisation step, i.e., user id sent as an input to the AccessManager is assumed to be of an authenticated user
2. The module has been built to offer flexibility with permissions, eg enable/disable permission, supply context- eg access based on location. These can be added as attributes to the Permission entity.
    
### Sample Commands
1. to assign a role to a user: ASSIGN {roleCode} {userId}
    * ASSIGN GSM 1
  
2. to remove a role from a user: REMOVE {roleCode} {userId}
    * REMOVE GSM 1
  
3. to check if a user has access: IS_AUTHORISED {userId} {actionCode} {resourceName}
    * IS_AUTHORISED 1 CREATE INVOICE 
  
4. to view all users
    * VIEW_USERS
    
5. to view all valid roles
    * VIEW_ROLES
    
6. to view all valid actions
    * VIEW_ACTIONS
    
7. to view all valid resources
    * VIEW_RESOURCES
    
8. to view user to roles mappings
    * VIEW_USER_TO_ROLES
    
9. to view role to permissions mappings
    * VIEW_ROLE_TO_PERMS
